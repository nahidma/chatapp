import React, { useContext } from "react";
import Login from "./components/Login/Login";
import Register from "./components/Login/Register";
import UserInf from "./components/UI/UserInf";
import { BrowserRouter, Routes, Route, Navigate } from "react-router-dom";
import { AuthContext } from "./context/Authcontext";

function App() {
  const currentuser = useContext(AuthContext);
  console.log(currentuser);

  const ProtectedRoute = ({ children }) => {
    if (!currentuser.currentUser) {
      return <Navigate to="/login" />;
    }
    return children;
  };
  return (
    <BrowserRouter>
      <Routes>
        <Route
          path="/"
          element={
            <ProtectedRoute>
              <UserInf />
            </ProtectedRoute>
          }
        />
        <Route path="login" element={<Login />} />
        <Route path="register" element={<Register />} />
      </Routes>
    </BrowserRouter>
  );
}

export default App;
