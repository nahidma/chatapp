import { createContext, useContext, useReducer } from "react";
import { AuthContext } from "./Authcontext";

export const ChatContext = createContext();

export const ChatContextProvider = (props) => {
  const { currentUser } = useContext(AuthContext);
  const initialState = {
    chatId: "null",
    user: {},
  };
  const chatReducer = (state, action) => {
    if (action.type === "CHANGE_USER") {
      return {
        user: action.payload,
        chatId:
          currentUser.uid > action.payload.uid
            ? currentUser.uid + action.payload.uid
            : action.payload.uid + currentUser.uid,
      };
    } else {
      return initialState;
    }
  };
  const [state, dispatch] = useReducer(chatReducer, initialState);
  return (
    <ChatContext.Provider value={{ data: state, dispatch }}>
      {props.children}
    </ChatContext.Provider>
  );
};
