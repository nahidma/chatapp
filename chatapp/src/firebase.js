import { initializeApp } from "firebase/app";
import { getAuth } from "firebase/auth";
import { getStorage } from "firebase/storage";
import { getFirestore } from "firebase/firestore";

const firebaseConfig = {
  apiKey: "AIzaSyAWd-20C--5gKt1jtOXl0mFj817COzx4nQ",
  authDomain: "chatapp-55e29.firebaseapp.com",
  projectId: "chatapp-55e29",
  storageBucket: "chatapp-55e29.appspot.com",
  messagingSenderId: "968904412465",
  appId: "1:968904412465:web:d61f832922ebb1fb57b410",
};

export const app = initializeApp(firebaseConfig);
export const auth = getAuth();
export const storage = getStorage();
export const db = getFirestore();
