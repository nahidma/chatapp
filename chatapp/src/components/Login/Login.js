import React, { useState, useContext } from "react";
import { useNavigate, Link } from "react-router-dom";
import { signInWithEmailAndPassword } from "firebase/auth";
import { auth } from "../../firebase";
import classes from "./Login.module.css";
const Login = () => {
  const [err, setErr] = useState(false);
  const navigate = useNavigate();

  const handleSubmit = async (e) => {
    e.preventDefault();
    const email = e.target[0].value;
    const password = e.target[1].value;

    try {
      await signInWithEmailAndPassword(auth, email, password);
      navigate("/");
      console.log("successfull");
    } catch (err) {
      setErr(true);
    }
  };
  return (
    <div className={classes.formContainer}>
      <div className={classes.formWrapper}>
        <span className={classes.logo}>Chatomic</span>
        <span className={classes.title}>Login</span>
        <form onSubmit={handleSubmit}>
          <input
            className={classes.input}
            required
            type="email"
            placeholder="email"
          />
          <input
            className={classes.input}
            required
            type="password"
            placeholder="password"
          />
          <button>Sign In</button>
          {err && <span>Something went wrong</span>}
        </form>
        <p>
          You don't have an account? <Link to="/register">register</Link>
        </p>
      </div>
    </div>
  );
};

export default Login;
