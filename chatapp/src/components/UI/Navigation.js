import React, { useContext } from "react";
import classes from "./Navigation.module.css";
import atomic from "../../assets/large_chatomic.png";
import notification from "../../assets/Chat-notification.png";
import { signOut } from "firebase/auth";
import { auth } from "../../firebase";
import { AuthContext } from "../../context/Authcontext";

const Navigation = () => {
  const currentUser = useContext(AuthContext);

  return (
    <div className={classes.navigation}>
      <img className={classes.icon} src={atomic} alt="logo" />
      <div className={classes.menu}>
        <img
          className={classes.notification}
          src={notification}
          alt="notifictaion icon"
        />
        <div className={classes.userdetails}>
          <img
            className={classes.profiledp}
            src={currentUser.currentUser.photoURL}
          ></img>
          <div className={classes.dropdown}>
            <div className={classes.username}>
              {currentUser.currentUser.displayName}
            </div>
            <div className={classes.dropdownContent}>
              <a href="/">Profile</a>
              <a
                className={classes.logout}
                onClick={() => {
                  signOut(auth);
                }}
              >
                Logout
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};
export default Navigation;
