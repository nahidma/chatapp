import React, { useContext } from "react";
import classes from "./Chatsection.module.css";
import UserBar from "./ChatsectionUI/Userbar";
import Messages from "./ChatsectionUI/Messages";
import MessageBar from "./ChatsectionUI/Messagebar";

const ChatSection = () => {
  return (
    <div className={classes.chatsection}>
      <UserBar />
      <Messages />
      <MessageBar />
    </div>
  );
};
export default ChatSection;
