import React, { useContext, useState } from "react";
import classes from "./Messagebar.module.css";
import { AuthContext } from "../../../context/Authcontext";
import { ChatContext } from "../../../context/ChatContext";
import {
  arrayUnion,
  doc,
  Timestamp,
  updateDoc,
  serverTimestamp,
} from "firebase/firestore";
import { db } from "../../../firebase";
import { v4 as uuid } from "uuid";

const MessageBar = () => {
  const [text, setText] = useState("");
  const { currentUser } = useContext(AuthContext);
  const { data } = useContext(ChatContext);
  const submitHandler = async (e) => {
    e.preventDefault();
    await updateDoc(doc(db, "chats", data.chatId), {
      messages: arrayUnion({
        id: uuid(),
        text,
        senderId: currentUser.uid,
        date: Timestamp.now(),
      }),
    });
    await updateDoc(doc(db, "userChats", currentUser.uid), {
      [data.chatId + ".date"]: serverTimestamp(),
    });

    await updateDoc(doc(db, "userChats", data.user.uid), {
      [data.chatId + ".date"]: serverTimestamp(),
    });
    setText("");
  };
  return (
    <div className={classes.messagebar}>
      <form className={classes.messageform}>
        <input
          className={classes.messageinput}
          type="text"
          placeholder="Type your message"
          onChange={(e) => setText(e.target.value)}
          value={text}
        ></input>
        <button className={classes.submit} onClick={submitHandler}>
          Enter
        </button>
      </form>
    </div>
  );
};
export default MessageBar;
