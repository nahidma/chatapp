import React, { useContext, useEffect, useState } from "react";
import { doc, onSnapshot } from "firebase/firestore";
import classes from "./Messages.module.css";
import Message from "./Message";
import { ChatContext } from "../../../context/ChatContext";
import { db } from "../../../firebase";
const Messages = (props) => {
  const [messages, setMessages] = useState([]);
  const { data } = useContext(ChatContext);
  useEffect(() => {
    const unsub = onSnapshot(doc(db, "chats", data.chatId), (doc) => {
      doc.exists() && setMessages(doc.data().messages);
    });
    return () => {
      unsub();
    };
  }, [data.chatId]);
  console.log(messages);

  return (
    <div className={classes.messages}>
      {messages.map((message) => {
        return <Message message={message} key={message.id} />;
      })}
    </div>
  );
};
export default Messages;
