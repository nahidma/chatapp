import React, { useContext } from "react";
import classes from "./Userbar.module.css";
import dp from "../../../assets/profileimage.png";
import { ChatContext } from "../../../context/ChatContext";

const UserBar = (props) => {
  const { data } = useContext(ChatContext);
  console.log(data);
  const userImage = `${data.chatId !== "null" ? classes.image : classes.none}`;
  const userName = `${
    data.chatId !== "null" ? classes.username : classes.none
  }`;
  const userClearchat = `${
    data.chatId !== "null" ? classes.clearChat : classes.none
  }`;
  return (
    <div className={classes.userbar}>
      {/* <div className={classes.userdp}></div> */}
      <img className={userImage} src={data.user.photoURL} alt="" />
      <span className={userName}>{data.user?.displayName}</span>
      <div className={userClearchat}>
        {data.chatId !== "null" ? "Clear Chat" : ""}
      </div>
    </div>
  );
};
export default UserBar;
