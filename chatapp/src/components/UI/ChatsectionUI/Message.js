import React, { useContext } from "react";
import dp from "../../../assets/profileimage.png";
import { AuthContext } from "../../../context/Authcontext";
import { ChatContext } from "../../../context/ChatContext";
import classes from "./Message.module.css";
const Message = (props) => {
  const { currentUser } = useContext(AuthContext);
  const { data } = useContext(ChatContext);

  return (
    <div
      className={`${classes.message} ${
        props.message.senderId === currentUser.uid && classes.owner
      }`}
    >
      <div className={classes.messageinfo}>
        <img
          className={classes.messagedp}
          src={
            props.message.senderId === currentUser.uid
              ? currentUser.photoURL
              : data.user.photoURL
          }
          alt=""
        />
        <span className={classes.messagetime}>Just now</span>
      </div>
      <div className={classes.messagecontent}>
        <p className={classes.messagetext}>{props.message.text}</p>
      </div>
    </div>
  );
};
export default Message;
