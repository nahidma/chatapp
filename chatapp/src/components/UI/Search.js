import React, { useState, useContext } from "react";
import classes from "./Search.module.css";
import lens from "../../assets/search-lens.png";
import dp from "../../assets/profileimage.png";
import {
  collection,
  query,
  where,
  getDocs,
  setDoc,
  doc,
  updateDoc,
  serverTimestamp,
  getDoc,
} from "firebase/firestore";
import { db } from "../../firebase";
import { ErrorFactory } from "@firebase/util";
import { AuthContext } from "../../context/Authcontext";

const Search = () => {
  const [username, setUsername] = useState("");
  const [user, setUser] = useState(null);
  const [err, setErr] = useState(false);
  const { currentUser } = useContext(AuthContext);
  const nameHandler = (e) => {
    setUsername(e.target.value);
  };
  const serachHandler = async () => {
    const usersRef = collection(db, "users");
    const q = query(usersRef, where("displayName", "==", username));
    try {
      const querySnapshot = await getDocs(q);
      console.log(querySnapshot.empty);
      if (!querySnapshot.empty) {
        querySnapshot.forEach((doc) => {
          setErr(false);
          setUser(doc.data());
        });
      } else {
        setUser(null);
        throw new Error("User not found");
      }
    } catch (err) {
      console.log(err);
      setErr(true);
    }
  };
  const keyHandler = (e) => {
    e.key === "Enter" && serachHandler();
  };
  const selectHandler = async () => {
    const combinedId =
      currentUser.uid > user.uid
        ? currentUser.uid + user.uid
        : user.uid + currentUser.uid;
    try {
      const res = await getDoc(doc(db, "chats", combinedId));

      if (!res.exists()) {
        await setDoc(doc(db, "chats", combinedId), { messages: [] });

        await updateDoc(doc(db, "userChats", currentUser.uid), {
          [combinedId + ".userInfo"]: {
            uid: user.uid,
            displayName: user.displayName,
            photoURL: user.photoURL,
          },
          [combinedId + ".date"]: serverTimestamp(),
        });

        await updateDoc(doc(db, "userChats", user.uid), {
          [combinedId + ".userInfo"]: {
            uid: currentUser.uid,
            displayName: currentUser.displayName,
            photoURL: currentUser.photoURL,
          },
          [combinedId + ".date"]: serverTimestamp(),
        });
      }
    } catch (err) {
      setErr(true);
    }
    setUser(null);
    setUsername("");
  };
  const clearHandler = () => {
    setUser(null);
    setErr(false);
  };
  return (
    <div className={classes.searchbar}>
      <img src={lens} className={classes.lens} alt="serach-lens" />
      <input
        type="text"
        className={classes.search}
        placeholder="Search Users"
        onKeyDown={keyHandler}
        onChange={nameHandler}
        value={username}
        onClick={clearHandler}
      />
      {err && <span className={classes.error}>User not found</span>}
      {user && (
        <div className={classes.usercard} onClick={selectHandler}>
          <img className={classes.image} src={user.photoURL} alt="" />
          <span className={classes.username}>{user.displayName}</span>
          {/* <div className={classes.loading}>Loading...</div> */}
        </div>
      )}
    </div>
  );
};
export default Search;
