import React from "react";
import classes from "./ChatUi.module.css";
import Users from "./Users";
import ChatSection from "./Chatsection";

const ChatUi = () => {
  return (
    <div className={classes.ui}>
      <Users />
      <ChatSection />
    </div>
  );
};
export default ChatUi;
