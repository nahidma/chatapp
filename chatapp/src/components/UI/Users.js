import React, { useContext, useState, useEffect } from "react";
import classes from "./Users.module.css";
import Search from "./Search";
import { doc, onSnapshot } from "firebase/firestore";
import { AuthContext } from "../../context/Authcontext";
import { db } from "../../firebase";
import { ChatContext } from "../../context/ChatContext";
const Users = () => {
  const [chats, setChats] = useState([]);
  const { currentUser } = useContext(AuthContext);
  const { dispatch } = useContext(ChatContext);
  useEffect(() => {
    const getChats = () => {
      const unsub = onSnapshot(doc(db, "userChats", currentUser.uid), (doc) => {
        setChats(doc.data());
      });
      return () => {
        unsub();
      };
    };
    currentUser.uid && getChats();
  }, [currentUser.uid]);
  const userSelectHandler = (user) => {
    dispatch({ type: "CHANGE_USER", payload: user });
  };

  return (
    <>
      <div className={classes.list}>
        <Search />
        <div className={classes.chats}>
          {Object.entries(chats)
            .sort((a, b) => b[1].date - a[1].date)
            .map((chat) => (
              <div
                className={classes.userchats}
                key={chat[0]}
                onClick={() => {
                  userSelectHandler(chat[1].userInfo);
                }}
              >
                <img
                  className={classes.image}
                  src={chat[1].userInfo.photoURL}
                  alt=""
                />
                <span className={classes.username}>
                  {chat[1].userInfo.displayName}
                </span>
              </div>
            ))}
        </div>
      </div>
    </>
  );
};
export default Users;
