import React from "react";
import classes from "./UserInf.module.css";
import Navigation from "./Navigation";
import ChatUi from "./ChatUi";
import InnerSection from "../Layout/Innersection";
import InnerCard from "../Layout/Innercard";

const UserInf = () => {
  return (
    <div className={classes.outer}>
      <InnerSection>
        <InnerCard>
          <Navigation />
          <ChatUi />
        </InnerCard>
      </InnerSection>
    </div>
  );
};
export default UserInf;
