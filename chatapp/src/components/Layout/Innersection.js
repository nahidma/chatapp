import React from "react";
import classes from "./Innersection.module.css";
const InnerSection = (props) => {
  return <div className={classes.innersection}>{props.children}</div>;
};
export default InnerSection;
