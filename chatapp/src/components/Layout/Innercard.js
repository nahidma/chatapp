import React from "react";
import classes from "./Innercard.module.css";
const InnerCard = (props) => {
  return <div className={classes.innercard}>{props.children}</div>;
};
export default InnerCard;
